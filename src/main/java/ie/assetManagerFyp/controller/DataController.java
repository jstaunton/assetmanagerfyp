package ie.assetManagerFyp.controller;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class DataController {
	private static Logger logger = LoggerFactory.getLogger(DataController.class);
	
	public List<Object> getStatusById(String id) {
		 try {
		        RestTemplate restTemplate = new RestTemplate();
		        String URL = "http://localhost:8080/api/sensorStatus/" + id;
		        String userName = "";
		        String password = "";
		        HttpHeaders headers = createHeaders(userName, password);
		        
		        ParameterizedTypeReference<List<Object>> myData = new ParameterizedTypeReference<List<Object>>(){};
		        ResponseEntity<List<Object>> responseEntity = restTemplate.exchange(URL, HttpMethod.GET, new HttpEntity<>(headers), myData);
		        List<Object> data = responseEntity.getBody();
		        logger.info("getStatus: --------------------------------------------------");
		        logger.info("data length: " + Integer.toString(data.size()));
		        if(data.size() > 0) {
		        	logger.info(data.get(0).toString());
		        }
//		        for(String reading : data) {
//		        	logger.info("reading: " + reading);
//		        }
		        
		        return data;
		 }
		catch (HttpClientErrorException ex) {	// all 4xx errors codes
           logger.info("dataController exception", ex.getMessage());
           return null;
       }
	}
	
	private HttpHeaders createHeaders(String userName, String password) {

		return new HttpHeaders() {
			{
				String auth = userName + ":" + password;
				byte[] encodeStringIntoBytes = auth.getBytes(StandardCharsets.UTF_8);
				byte[] encodedAuth = Base64.encodeBase64(encodeStringIntoBytes);
				String authHeader = "Basic " + new String(encodedAuth);
				log.info("INFO...{}", authHeader);
				setContentType(MediaType.APPLICATION_JSON);
				// setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
				set(HttpHeaders.AUTHORIZATION, authHeader);
			}
		};
	}
}
