package ie.assetManagerFyp;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ie.assetManagerFyp.controller.DataController;

@SpringBootApplication
public class AssetManagerFypApplication implements CommandLineRunner{
	private static Logger logger = LoggerFactory.getLogger(AssetManagerFypApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(AssetManagerFypApplication.class, args);
	}

	@Autowired
	private DataController dataController;
	
	public String id = "sensor1";
	@Override
	public void run(String... args) throws Exception {
		
		while (true) {
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			List<Object> data = dataController.getStatusById(id);
			
			if(data != null) {
				for(Object reading : data) {
		        	logger.info("reading: " + reading.toString());
		        }
			}
			
			List<Object> data1 = dataController.getStatusById("sensor2");
			
			if(data1 != null) {
				for(Object reading : data1) {
		        	logger.info("reading: " + reading.toString());
		        }
			}
		}
		
	}

}
